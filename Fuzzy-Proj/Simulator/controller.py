from math import degrees

from fuzzy.storage.fcl.Reader import Reader


def fuzzify_pa(state, x):
    while x < 0: x += 360
    while x >= 360: x -= 360
    if state == 'up_more_right': return x / 30. if 0 <= x < 30 else 2 - x / 30. if 30 <= x < 60 else 0
    if state == 'up_right': return -1 + x / 30. if 30 <= x < 60 else 3 - x / 30. if 60 <= x < 90 else 0
    if state == 'up': return -2 + x / 30. if 60 <= x < 90 else 4 - x / 30. if 90 <= x < 120 else 0
    if state == 'up_left': return -3 + x / 30. if 90 <= x < 120 else 5 - x / 30. if 120 <= x < 150 else 0
    if state == 'up_more_left': return -4 + x / 30. if 120 <= x < 150 else 6 - x / 30. if 150 <= x < 180 else 0
    if state == 'down_more_left': return -6 + x / 30. if 180 <= x < 210 else 8 - x / 30. if 210 <= x < 240 else 0
    if state == 'down_left': return -7 + x / 30. if 210 <= x < 240 else 9 - x / 30. if 240 <= x < 270 else 0
    if state == 'down': return -8 + x / 30. if 240 <= x < 270 else 10 - x / 30 if 270 <= x < 300 else 0
    if state == 'down_right': return -9 + x / 30. if 270 <= x < 300 else 11 - x / 30. if 300 <= x < 330 else 0
    if state == 'down_more_right': return -10 + x / 30. if 300 <= x < 330 else 12 - x / 30. if 330 <= x < 360 else 0


def fuzzify_pv(state, x):
    if state == 'cw_fast': return 1 if x < -200 else -1 - x / 100. if -200 <= x < -100 else 0
    if state == 'cw_slow': return 2 + x / 100. if -200 <= x < -100 else -x / 100. if -100 <= x < 0 else 0
    if state == 'stop': return 1 + x / 100. if -100 <= x < 0 else 1 - x / 100. if 0 <= x < 100 else 0
    if state == 'ccw_slow': return x / 100. if 0 <= x < 100 else 2 - x / 100. if 100 <= x < 200 else 0
    if state == 'ccw_fast': return 1 if x >= 200 else -1 + x / 100. if 100 <= x < 200 else 0


def fuzzify_force(state, x):
    if state == 'left_fast': return 5 + x / 20. if -100 < x <= -80 else -3 - x / 20. if -80 < x <= -60 else 0
    if state == 'left_slow': return 4 + x / 20. if -80 < x <= -60 else -x / 60. if -60 < x <= 0 else 0
    if state == 'stop': return 1 + x / 60. if -60 < x <= 0 else 1 - x / 60. if 0 < x <= 60 else 0
    if state == 'right_slow': return 4 + x / 60. if 0 < x <= 60 else 4 - x / 20. if 60 < x <= 80 else 0
    if state == 'right_fast': return -3 + x / 20. if 60 < x <= 80 else 5 - x / 20. if 80 < x <= 100 else 0


class FuzzyController:

    def __init__(self, fcl_path):
        self.system = Reader().load_from_file(fcl_path)
        self.x = -10
        self.pv = {}
        self.pa = {}
        self.fuzzy_force = {}

    @staticmethod
    def _make_input(world):
        return dict(
            cp=world.x,
            cv=world.v,
            pa=degrees(world.theta),
            pv=degrees(world.omega)
        )

    @staticmethod
    def _make_output():
        return dict(force=0.)

    def inference(self, state):
        if state == 'stop':
            return max(
                max(min(self.pa['up'], self.pv['stop']), min(self.pa['up_right'], self.pv['ccw_slow']),
                    min(self.pa['up_left'], self.pv['cw_slow'])),  # 0
                min(self.pa['down_more_right'], self.pv['cw_slow']),  # 10
                min(self.pa['down_more_left'], self.pv['ccw_slow']),  # 12
                min(self.pa['down_more_right'], self.pv['ccw_fast']),  # 13
                min(self.pa['down_more_right'], self.pv['cw_fast']),  # 14
                min(self.pa['down_more_left'], self.pv['cw_fast']),  # 15
                min(self.pa['down_more_left'], self.pv['ccw_fast']),  # 16
                min(self.pa['down_right'], self.pv['ccw_fast']),  # 21
                min(self.pa['down_left'], self.pv['cw_fast']),  # 23
                min(self.pa['down'], self.pv['cw_fast']),  # 36
                min(self.pa['down'], self.pv['ccw_fast']),  # 37
                min(self.pa['up'], self.pv['stop']))  # 42
        if state == 'right_fast':
            return max(
                min(self.pa['up_more_right'], self.pv['ccw_slow']),  # 1
                min(self.pa['up_more_right'], self.pv['cw_slow']),  # 2
                min(self.pa['down_more_right'], self.pv['ccw_slow']),  # 9
                min(self.pa['down_right'], self.pv['ccw_slow']),  # 17
                min(self.pa['down_right'], self.pv['cw_slow']),  # 18
                min(self.pa['up_right'], self.pv['cw_slow']),  # 26
                min(self.pa['up_right'], self.pv['stop']),  # 27
                min(self.pa['up_right'], self.pv['cw_fast']),  # 32
                min(self.pa['up_left'], self.pv['cw_fast']),  # 33
                min(self.pa['down'], self.pv['stop']),  # 35
                min(self.pa['up'], self.pv['cw_fast']))  # 41
        if state == 'right_slow':
            return max(
                min(self.pa['up_more_left'], self.pv['cw_fast']),  # 7
                min(self.pa['down_right'], self.pv['cw_fast']),  # 22
                min(self.pa['up_right'], self.pv['ccw_slow']),  # 25
                min(self.pa['up'], self.pv['cw_slow']))  # 40
        if state == 'left_fast':
            return max(
                min(self.pa['up_more_left'], self.pv['cw_slow']),  # 3
                min(self.pa['up_more_left'], self.pv['ccw_slow']),  # 4
                min(self.pa['up_more_left'], self.pv['ccw_fast']),  # 8
                min(self.pa['down_more_left'], self.pv['cw_slow']),  # 11
                min(self.pa['down_left'], self.pv['cw_slow']),  # 19
                min(self.pa['down_left'], self.pv['ccw_slow']),  # 20
                min(self.pa['up_left'], self.pv['ccw_slow']),  # 29
                min(self.pa['up_left'], self.pv['stop']),  # 30
                min(self.pa['up_right'], self.pv['ccw_fast']),  # 31
                min(self.pa['up_left'], self.pv['ccw_fast']),  # 34
                min(self.pa['up'], self.pv['ccw_fast']))  # 39
        if state == 'left_slow':
            return max(
                min(self.pa['up_more_right'], self.pv['ccw_fast']),  # 5
                min(self.pa['down_left'], self.pv['ccw_fast']),  # 24
                min(self.pa['up_left'], self.pv['cw_slow']),  # 28
                min(self.pa['up'], self.pv['ccw_slow']))  # 38

    def defuzzify(self, x):
        def force_membership_value(state):
            return min(fuzzify_force(state, x), self.fuzzy_force[state])
        return max(
            force_membership_value('left_fast'),
            force_membership_value('left_slow'),
            force_membership_value('stop'),
            force_membership_value('right_slow'),
            force_membership_value('right_fast'))

    def calculate(self, _input):
        self.pa = dict(
            up_more_right=fuzzify_pa('up_more_right', _input['pa']),
            up_right=fuzzify_pa('up_right', _input['pa']),
            up=fuzzify_pa('up', _input['pa']),
            up_left=fuzzify_pa('up_left', _input['pa']),
            up_more_left=fuzzify_pa('up_more_left', _input['pa']),
            down_more_left=fuzzify_pa('down_more_left', _input['pa']),
            down_left=fuzzify_pa('down_left', _input['pa']),
            down=fuzzify_pa('down', _input['pa']),
            down_right=fuzzify_pa('down_right', _input['pa']),
            down_more_right=fuzzify_pa('down_more_right', _input['pa']))

        self.pv = dict(
            cw_fast=fuzzify_pv('cw_fast', _input['pv']),
            cw_slow=fuzzify_pv('cw_slow', _input['pv']),
            stop=fuzzify_pv('stop', _input['pv']),
            ccw_fast=fuzzify_pv('ccw_fast', _input['pv']),
            ccw_slow=fuzzify_pv('ccw_slow', _input['pv']))

        self.fuzzy_force = dict(
            left_fast=self.inference('left_fast'),
            left_slow=self.inference('left_slow'),
            stop=self.inference('stop'),
            right_slow=self.inference('right_slow'),
            right_fast=self.inference('right_fast'))
        return self.center_of_mass(-100, 100, 0.1)

    def center_of_mass(self, a, b, h):
        def function(x):
            return self.defuzzify(x) * x
        sum1 = 0.
        sum2 = 0.
        while b > a:
            sum1 += function(a) * h
            sum2 += self.defuzzify(a) * h
            a += h
        return sum1 / sum2

    def decide(self, world):
        return self.calculate(self._make_input(world))
