import numpy as np
import math
import random
import pickle
import time
import matplotlib.pyplot as plt
from functools import reduce

# Define number of neurons in each layer.
# layers[0] ==> input layer
# layers[1] ==> first hidden layer
# layers[2] ==> second hidden layer
# layers[3] ==> output layer
layers = [102, 150, 60, 4]

weights = []
biases = []
neuron_values = []
z = []  # Z = WX + B


def initial_weight_and_biases():
    global weights, biases, neuron_values
    for k in range(0, 3):
        weights.append(np.random.normal(0, 1, size=(layers[k + 1], layers[k])))
        biases.append(np.zeros(shape=(layers[k + 1], 1)))
        neuron_values.append(np.zeros(shape=(layers[k + 1], 1)))
        z.append(np.zeros(shape=(layers[k + 1], 1)))


def feed_forwarding(data, vectorized=True):
    global weights, biases
    if vectorized:
        z[0] = weights[0] @ data + biases[0]
        neuron_values[0] = sigmoid(z[0])
        z[1] = weights[1] @ neuron_values[0] + biases[1]
        neuron_values[1] = sigmoid(z[1])
        z[2] = weights[2] @ neuron_values[1] + biases[2]
        neuron_values[2] = sigmoid(z[2])
    else:
        for i in range(len(z[0])):
            z[0][i] = 0
            for j in range(weights[0].shape[1]):
                z[0][i] += weights[0][i, j] * data[j] + biases[0][i]
            neuron_values[0][i] = sigmoid(z[0][i])

        for i in range(len(z[1])):
            z[1][i] = 0
            for j in range(weights[1].shape[1]):
                z[1][i] += weights[1][i, j] * neuron_values[0][j] + biases[1][i]
            neuron_values[1][i] = sigmoid(z[1][i])

        for i in range(len(z[2])):
            z[2][i] = 0
            for j in range(weights[2].shape[1]):
                z[2][i] += weights[2][i, j] * neuron_values[1][j] + biases[2][i]
            neuron_values[2][i] = sigmoid(z[2][i])
    return neuron_values[2]


def sigmoid(x):
    return 1 / (1 + math.e ** -x)


def sigmoid_prime(x):
    return sigmoid(x) * (1 - sigmoid(x))


def load_dataset():
    # loading training set features
    f = open("Datasets/train_set_features.pkl", "rb")
    train_set_features2 = pickle.load(f)
    f.close()
    # reducing feature vector length
    features_stds = np.std(a=train_set_features2, axis=0)
    train_set_features = train_set_features2[:, features_stds > 52.3]
    # changing the range of data between 0 and 1
    train_set_features = np.divide(train_set_features, train_set_features.max())
    # loading training set labels
    f = open("Datasets/train_set_labels.pkl", "rb")
    train_set_labels = pickle.load(f)
    f.close()
    # ------------
    # loading test set features
    f = open("Datasets/test_set_features.pkl", "rb")
    test_set_features2 = pickle.load(f)
    f.close()
    # reducing feature vector length
    features_stds = np.std(a=test_set_features2, axis=0)
    test_set_features = test_set_features2[:, features_stds > 48]
    # changing the range of data between 0 and 1
    test_set_features = np.divide(test_set_features, test_set_features.max())
    # loading test set labels
    f = open("Datasets/test_set_labels.pkl", "rb")
    test_set_labels = pickle.load(f)
    f.close()
    # ------------
    # preparing our training and test sets - joining datasets and labels
    train_set = []
    test_set = []
    for i in range(len(train_set_features)):
        label = np.array([0, 0, 0, 0])
        label[int(train_set_labels[i])] = 1
        label = label.reshape(4, 1)
        train_set.append((train_set_features[i].reshape(102, 1), label))
    for i in range(len(test_set_features)):
        label = np.array([0, 0, 0, 0])
        label[int(test_set_labels[i])] = 1
        label = label.reshape(4, 1)
        test_set.append((test_set_features[i].reshape(102, 1), label))
    # shuffle
    random.shuffle(train_set)
    random.shuffle(test_set)
    # print(len(train_set))  # 1962
    # print(len(test_set))  # 662
    return train_set, test_set


def back_propagation(batch, vectorized=True):
    cost = 0
    correct_res = 0
    grad_w = []
    grad_b = []
    for k in range(0, 3):
        grad_w.append(np.zeros_like(weights[k]))
        grad_b.append(np.zeros_like(biases[k]))
    for value in batch:
        if vectorized:
            feed_forwarding(value[0])
            gradient_b3 = 2 * sigmoid_prime(z[2]) * (neuron_values[2] - value[1])
            gradient_w3 = gradient_b3 @ np.transpose(neuron_values[1])
            gradient_a2 = np.transpose(weights[2]) @ gradient_b3
            # ----------------------------------------------------------------------------
            gradient_b2 = gradient_a2 * sigmoid_prime(z[1])
            gradient_w2 = gradient_b2 @ np.transpose(neuron_values[0])
            gradient_a1 = np.transpose(weights[1]) @ gradient_b2
            # ----------------------------------------------------------------------------
            gradient_b1 = gradient_a1 * sigmoid_prime(z[0])
            gradient_w1 = gradient_b1 @ np.transpose(value[0])
            # ----------------------------------------------------------------------------
        else:
            feed_forwarding(value[0])
            gradient_b3 = np.zeros_like(biases[2])
            for i in range(0, len(biases[2])):
                gradient_b3[i] += 2 * sigmoid_prime(z[2][i]) * (neuron_values[2][i] - value[1][i])
            # ----------------------------------------------------------------------------
            gradient_w3 = np.zeros_like(weights[2])
            for i in range(0, len(weights[2])):
                for j in range(len(weights[2][i])):
                    gradient_w3[i, j] += gradient_b3[i] * neuron_values[1][j]
            # ----------------------------------------------------------------------------
            gradient_a2 = np.zeros_like(neuron_values[1])
            for i in range(len(gradient_a2)):
                for j in range(weights[2].shape[0]):
                    gradient_a2[i] += weights[2][j, i] * gradient_b3[j]
            # ----------------------------------------------------------------------------
            gradient_b2 = np.zeros_like(biases[1])
            for i in range(len(gradient_a2)):
                gradient_b2[i] += gradient_a2[i] * sigmoid_prime(z[1][i])
            # ----------------------------------------------------------------------------
            gradient_w2 = np.zeros_like(weights[1])
            for i in range(len(gradient_w2)):
                for j in range(len(gradient_w2[i])):
                    gradient_w2[i, j] += gradient_b2[i] * neuron_values[0][j]
            # ----------------------------------------------------------------------------
            gradient_a1 = np.zeros_like(neuron_values[0])
            for i in range(len(gradient_a1)):
                for j in range(weights[1].shape[0]):
                    gradient_a1[i] += weights[1][j, i] * gradient_b2[j]
            # ----------------------------------------------------------------------------
            gradient_b1 = np.zeros_like(biases[0])
            for i in range(len(gradient_b1)):
                gradient_b1 += gradient_a1[i] * sigmoid_prime(z[0][i])
            # ----------------------------------------------------------------------------
            gradient_w1 = np.zeros_like(weights[0])
            for i in range(len(gradient_w1)):
                for j in range(len(gradient_w1[i])):
                    gradient_w1[i, j] += gradient_b1[i] * value[0][j]
        grad_w[0] += gradient_w1
        grad_w[1] += gradient_w2
        grad_w[2] += gradient_w3
        grad_b[0] += gradient_b1
        grad_b[1] += gradient_b2
        grad_b[2] += gradient_b3
        cost += np.sum((neuron_values[2] - value[1]) ** 2)
        res = neuron_values[2]
        res = np.intc(np.divide(res, res.max()))
        correct_res += 1 if (res == value[1]).all() else 0
    return grad_w, grad_b, cost, correct_res


def validation_cost(validate_set):
    cost = 0
    for value in validate_set:
        cost += np.sum((feed_forwarding(value[0]) - value[1]) ** 2)
    return cost


def train_model(train_set, validate_set, epoch, batch_size, learning_rate):
    print("**************************************(START TO TRAIN MODEL)**************************************")
    global weights, biases
    costs = []
    validation_costs = []
    for i in range(epoch):
        epoch_cost = 0
        epoch_correct_res = 0
        random.shuffle(train_set)
        batched_train = [train_set[j:j + batch_size] for j in range(0, len(train_set), batch_size)]
        start_time = time.time()
        for mini_batch in batched_train:
            batch_size = len(mini_batch)
            grad_w, grad_b, cost, correct_res = back_propagation(mini_batch)
            epoch_cost += cost
            epoch_correct_res += correct_res
            for k in range(0, 3):
                weights[k] -= learning_rate * grad_w[k] / batch_size
                biases[k] -= learning_rate * grad_b[k] / batch_size
        costs.append(epoch_cost / len(train_set))
        val_cost = validation_cost(validate_set)
        validation_costs.append(val_cost / len(validate_set))
        print(f'EPOCH {i + 1:02d} :: '
              f'(cost={round(epoch_cost / len(train_set), 3)}), '
              f'accuracy={epoch_correct_res}/{len(train_set)}={round(epoch_correct_res * 100 / len(train_set), 3)}%, '
              f'validation_cost={round(val_cost, 3)}, '
              f'duration_time={round((time.time() - start_time), 3)}seconds')
        print("----------------------------------------------------------------------------------------------------")
    print("*************************************(FINISH TRAINING MODEL)*************************************")
    return costs, validation_costs


def test_model_without_train():
    print("************************************(RESULT WITHOUT TRAINING)************************************")
    initial_weight_and_biases()
    train_set, test_set = load_dataset()
    correct_res = 0
    train_set = train_set[:200]
    for data in train_set:
        res = feed_forwarding(data[0])
        res = np.intc(np.divide(res, res.max()))
        correct_res += 1 if (res == data[1]).all() else 0
    print(f'ACCURACY :: {correct_res}/{len(train_set)} = {round(correct_res * 100 / len(train_set), 4)}%')
    print("**************************************************************************************************\n")


def test_model_with_train():
    initial_weight_and_biases()
    train_set, validation_set = load_dataset()
    # validation_set = validation_set[:100]
    # train_set = train_set[:200]
    epoch = 20
    batch_size = 16
    learning_rate = 0.6
    costs, validation_costs = train_model(train_set, validation_set, epoch, batch_size, learning_rate)
    correct_res = 0
    for data in validation_set:
        res = feed_forwarding(data[0])
        res = np.intc(np.divide(res, res.max()))
        if (res == data[1]).all():
            correct_res += 1
    plt.style.use('dark_background')
    plt.plot(range(1, len(costs) + 1), costs)
    plt.plot(range(1, len(validation_costs) + 1), validation_costs)
    plt.title(f"EPOCH = {epoch}, BATCH_SIZE={batch_size}, LEARNING_RATE={learning_rate}")
    plt.legend(('training cost', 'validation cost'))
    plt.show()
    print(f'ACCURACY :: {correct_res}/{len(validation_set)} = {round(correct_res * 100 / len(validation_set), 4)}%')
    return costs[len(costs) - 1], validation_costs[len(validation_costs) - 1]


if __name__ == '__main__':
    test_model_with_train()
    # costs_avg = []
    # val_costs = []
    # for i in range(0, 1):
    #     cost, validate_cost = test_model_with_train()
    #     costs_avg.append(cost)
    #     print(cost)
    #     print(validate_cost)
    #     val_costs.append(validate_cost)
    # print(f'Average train cost = {round(reduce(lambda a, b: a + b, costs_avg) / len(costs_avg), 5)} '
    #       f'|| Average validation cost '
    #       f'= {round(reduce(lambda a, b: a + b, val_costs) / len(val_costs), 5)}')
