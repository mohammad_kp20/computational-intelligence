import copy
import numpy as np
import random

import player
from player import Player


def add_noise(matrix, noise):
    matrix_copy = copy.deepcopy(matrix)
    noise = np.random.normal(0, noise, matrix.shape)
    matrix_copy += noise
    return matrix_copy


def calculate_probabilities(players):
    total_fitness = sum(p.fitness for p in players)
    probabilities = []
    for p in players:
        probabilities.append(p.fitness / total_fitness)
    for i in range(1, len(players)):
        probabilities[i] += probabilities[i - 1]
    return probabilities


def roulette_wheel_selection(players):
    population_fitness = sum([p.fitness for p in players])
    chromosome_probabilities = [chromosome.fitness / population_fitness for chromosome in players]
    return np.random.choice(players, p=chromosome_probabilities)


class Evolution:
    def __init__(self):
        self.game_mode = "Neuroevolution"
        self.max = []
        self.min = []
        self.avg = []

    def next_population_selection(self, players, num_players):
        """
        Gets list of previous and current players (μ + λ) and returns num_players number of players based on their
        fitness value.

        :param players: list of players in the previous generation
        :param num_players: number of players that we return
        """
        # TODO (Implement top-k algorithm here)
        # TODO (Additional: Implement roulette wheel here)
        # TODO (Additional: Implement SUS here)

        # TODO (Additional: Learning curve)
        players = self.roulette_wheel(players)
        players = self.q_tournament(players)
        players.sort(key=lambda x: x.fitness, reverse=True)
        fitness_list = [p.fitness for p in players]
        print(f'Max={max(fitness_list)} , Min={min(fitness_list)} , avg={sum(fitness_list) / len(fitness_list)}')
        self.max.append(max(fitness_list))
        self.min.append(min(fitness_list))
        self.avg.append(sum(fitness_list) / len(fitness_list))
        return players[: num_players]

    @staticmethod
    def get_players_fitness(players):
        players_fitness = []
        for p in players:
            players_fitness.append(p.fitness)
        return players_fitness

    @staticmethod
    def new_fitness_function(fitness_arr):
        new_fitness_arr = []
        for fitness in fitness_arr:
            new_fitness_arr.append(fitness ** 2)
        return new_fitness_arr

    def roulette_wheel(self, players):
        probabilities = calculate_probabilities(players)
        selected = []
        for random_number in np.random.uniform(low=0, high=1, size=len(players)):
            for i, probability in enumerate(probabilities):
                if random_number <= probability:
                    selected.append(self.clone_player(players[i]))
                    break
        return selected

    def generate_new_population(self, num_players, prev_players=None):
        """
        Gets survivors and returns a list containing num_players number of children.

        :param num_players: Length of returning list
        :param prev_players: List of survivors
        :return: A list of children
        """
        first_generation = prev_players is None
        if first_generation:
            return [Player(self.game_mode) for _ in range(num_players)]
        else:
            # TODO ( Parent selection and child generation )
            # TODO ( Parent selection and child generation )
            prev_players_copy = prev_players.copy()
            players_fitness_arr = self.get_players_fitness(prev_players_copy)
            players_new_fitness_arr = self.new_fitness_function(players_fitness_arr)
            children = []
            parent = random.choices(prev_players_copy, weights=players_new_fitness_arr, k=300)
            # parent = self.roulette_wheel(prev_players)
            # parent = self.q_tournament(prev_players)
            for i in range(0, num_players, 2):
                child1, child2 = self.child_generation(parent[i], parent[i + 1])
                children.append(child1)
                children.append(child2)
        return children

    @staticmethod
    def q_tournament(prev_players, q=3):
        parents = []
        for _ in range(len(prev_players)):
            q_selected = np.random.choice(prev_players, q)
            parents.append(max(q_selected, key=lambda p: p.fitness))
        return parents

    def sus(self, players, num_players):
        interval_length = 1 - 1 / num_players
        intervals = np.linspace(0, interval_length, num_players)
        random_number = np.random.uniform(0, 1 / num_players, 1)
        intervals += random_number
        probabilities = calculate_probabilities(players)
        parents = []
        for interval in intervals:
            for i, probability in enumerate(probabilities):
                if interval < probability:
                    parents.append(self.clone_player(players[i]))
                    break
        return parents

    @staticmethod
    def mutate(child: player):
        if np.random.uniform(0, 1, 1)[0] <= 0.3:
            child.nn.weights[0] = add_noise(child.nn.weights[0], 0.2)
        if np.random.uniform(0, 1, 1)[0] <= 0.3:
            child.nn.biases[0] = add_noise(child.nn.biases[0], 0.2)
        if np.random.uniform(0, 1, 1)[0] <= 0.3:
            child.nn.weights[1] = add_noise(child.nn.weights[1], 0.2)
        if np.random.uniform(0, 1, 1)[0] <= 0.3:
            child.nn.biases[1] = add_noise(child.nn.biases[1], 0.2)

    def child_generation(self, parent_1: player, parent_2: player):
        child_1 = self.clone_player(parent_1)
        child_2 = self.clone_player(parent_1)
        child_1.nn.weights[0], child_2.nn.weights[0] = self.crossover1(parent_1.nn.weights[0], parent_2.nn.weights[0])
        child_1.nn.weights[1], child_2.nn.weights[1] = self.crossover1(parent_1.nn.weights[1], parent_2.nn.weights[1])
        child_1.nn.biases[0], child_2.nn.biases[0] = self.crossover1(parent_1.nn.biases[0], parent_2.nn.biases[0])
        child_1.nn.biases[1], child_2.nn.biases[1] = self.crossover1(parent_1.nn.biases[1], parent_2.nn.biases[1])
        self.mutate(child_1)
        self.mutate(child_2)
        return child_1, child_2

    @staticmethod
    def crossover(parent1, parent2):
        child_1 = np.copy(parent1)
        child_2 = np.copy(parent1)
        for i in range(len(parent1[0])):
            randIndex = random.randint(0, len(parent1[0]) - 1)
            child_1[:randIndex, i] = parent1[:randIndex, i]
            child_1[randIndex:, i] = parent2[randIndex:, i]
            child_2[:randIndex, i] = parent2[:randIndex, i]
            child_2[randIndex:, i] = parent1[randIndex:, i]
        return child_1, child_2

    @staticmethod
    def crossover1(parent1, parent2):
        child_1 = np.copy(parent1)
        child_2 = np.copy(parent1)
        for i in range(len(parent1)):
            randIndex = random.randint(0, len(parent1) - 1)
            child_1[i, :randIndex] = parent1[i, :randIndex]
            child_1[i, randIndex:] = parent2[i, randIndex:]
            child_2[i, :randIndex] = parent2[i, :randIndex]
            child_2[i, randIndex:] = parent1[i, randIndex:]
        return child_1, child_2

    def clone_player(self, player):
        """
        Gets a player as an input and produces a clone of that player.
        """
        new_player = Player(self.game_mode)
        new_player.nn = copy.deepcopy(player.nn)
        new_player.fitness = player.fitness
        return new_player
