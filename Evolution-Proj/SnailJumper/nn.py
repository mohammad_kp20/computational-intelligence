import math

import numpy as np


def relu(x: np.array):
    return np.maximum(x / 10, x)


class NeuralNetwork:

    def __init__(self, layers: list):
        """
        Neural Network initialization.
        Given layer_sizes as an input, you have to design a Fully Connected Neural Network architecture here.
        :param layers: A list containing neuron numbers in each layers. For example [3, 10, 2] means that there are
        3 neurons in the input layer, 10 neurons in the hidden layer, and 2 neurons in the output layer.
        """
        self.layers = layers
        self.weights = []
        self.biases = []
        self.neuron_values = []
        self.z = []

    @staticmethod
    def activation(x):
        """
        The activation function of our neural network, e.g., Sigmoid, ReLU.
        :param x: Vector of a layer in our network.
        :return: Vector after applying activation function.
        """
        return 1 / (1 + math.e ** -x)

    def forward(self, x, screen_width, screen_height):
        """
        Receives input vector as a parameter and calculates the output vector based on weights and biases.
        :param screen_width:
        :param screen_height:
        :param x: Input vector which is a numpy array.
        :return: Output vector
        """
        # x = np.array(x)
        # x = x.reshape(self.layers[0], 1)
        # x = self.normalize_input_layer(x)
        for i in range(0, len(self.layers) - 1):
            self.z[i] = self.weights[i] @ x + self.biases[i] if i == 0 else \
                self.weights[i] @ self.neuron_values[i - 1] + self.biases[i]
            self.neuron_values[i] = self.activation(self.z[i])
        return self.neuron_values[-1]

    def initial_weight_and_biases(self):
        for k in range(0, len(self.layers) - 1):
            self.weights.append(np.random.normal(0, 1, size=(self.layers[k + 1], self.layers[k])))
            self.biases.append(np.zeros(shape=(self.layers[k + 1], 1)))
            self.neuron_values.append(np.zeros(shape=(self.layers[k + 1], 1)))
            self.z.append(np.zeros(shape=(self.layers[k + 1], 1)))

    def normalize_input_layer(self, input_layer):
        input_layer = np.array(input_layer)
        input_layer = input_layer.reshape(self.layers[0], 1)
        return input_layer
